CREATE TABLE [platforms] (
	[platform_id] INTEGER NOT NULL PRIMARY KEY,
	[name] VARCHAR(1024)
)

CREATE TABLE [plats_grp] (
	[plat_grp_id] INTEGER NOT NULL PRIMARY KEY,
	[platform_id] INTEGER NOT NULL,
    CONSTRAINT [FK_plats_grp_plat] FOREIGN KEY([platform_id]) REFERENCES [platforms] ([platform_id])
)

CREATE TABLE [genres] (
	[genre_id] INTEGER NOT NULL PRIMARY KEY,
	[name] VARCHAR(256)
)

CREATE TABLE [genres_grp] (
	[genre_grp_id] INTEGER NOT NULL PRIMARY KEY,
	[genre_id] INTEGER NOT NULL,
    CONSTRAINT [FK_genres_grp_genre] FOREIGN KEY([genre_id]) REFERENCES [genres] ([genre_id])
)

CREATE TABLE [developers] (
	[developer_id] INTEGER NOT NULL PRIMARY KEY,
	[name] VARCHAR(1024)
)

CREATE TABLE [devs_grp] (
	[dev_grp_id] INTEGER NOT NULL PRIMARY KEY,
	[developer_id] INTEGER NOT NULL,
    CONSTRAINT [FK_dev_grp_dev] FOREIGN KEY([developer_id]) REFERENCES [developers] ([developer_id])
)

CREATE TABLE [publishers] (
	[publisher_id] INTEGER NOT NULL PRIMARY KEY,
	[name] VARCHAR(1024)
)

CREATE TABLE [pubs_grp] (
	[pub_grp_id] INTEGER NOT NULL PRIMARY KEY,
	[publisher_id] INTEGER NOT NULL
    CONSTRAINT [FK_pub_grp_pub] FOREIGN KEY([publisher_id]) REFERENCES [publishers] ([publisher_id])
)

CREATE TABLE [games] (
	[game_id] INTEGER NOT NULL PRIMARY KEY,
	[title] VARCHAR(1024) NOT NULL,
	[released_on] DATE,
	[price] DECIMAL(6, 2) NOT NULL,
    [pub_grp_id] INTEGER,
    [plat_grp_id] INTEGER,
    [dev_grp_id]  INTEGER,
    [genre_grp_id] INTEGER,
    CONSTRAINT [FK_game_pub_grp] FOREIGN KEY([pub_grp_id]) REFERENCES [pubs_grp] ([pub_grp_id]),
    CONSTRAINT [FK_game_plat_grp] FOREIGN KEY([plat_grp_id]) REFERENCES [plats_grp] ([plat_grp_id]),
    CONSTRAINT [FK_game_dev_grp] FOREIGN KEY([dev_grp_id]) REFERENCES [devs_grp] ([dev_grp_id]),
    CONSTRAINT [FK_game_genre_grp] FOREIGN KEY([genre_grp_id]) REFERENCES [genres_grp] ([genre_grp_id]),
)

CREATE TABLE [users] (
     [user_id] INTEGER NOT NULL PRIMARY KEY,
	 [name] varchar(128),
	 [gender] varchar(6)
)

CREATE TABLE [sessions] (
	[session_id] INTEGER NOT NULL PRIMARY KEY,
	[user_id] INTEGER,
	[device] VARCHAR(32),
	[os_platform] VARCHAR(32),
	[browser] VARCHAR(32),
	[created_at] DATETIME,
    CONSTRAINT [FK_sess_usr] FOREIGN KEY([user_id]) REFERENCES [users] ([user_id]),
)

CREATE TABLE [visits] (
	[visit_id] INTEGER NOT NULL PRIMARY KEY,
	[session_id] INTEGER NOT NULL,
	[user_id] INTEGER NOT NULL,
    [start_time] DATETIME,
	[dur_secs] INTEGER,
    [game_id] INTEGER NOT NULL,
    [pub_grp_id] INTEGER,
	[plat_grp_id] INTEGER,
	[dev_grp_id]  INTEGER,
	[genre_grp_id] INTEGER,
    CONSTRAINT [FK_visit_sess] FOREIGN KEY([session_id]) REFERENCES [sessions] ([session_id]),
    CONSTRAINT [FK_visit_usr] FOREIGN KEY([user_id]) REFERENCES [users] ([user_id]),
    CONSTRAINT [FK_visit_game] FOREIGN KEY([game_id]) REFERENCES [games] ([game_id]),
    CONSTRAINT [FK_visit_pub_grp] FOREIGN KEY([pub_grp_id]) REFERENCES [pubs_grp] ([pub_grp_id]),
    CONSTRAINT [FK_visit_plat_grp] FOREIGN KEY([plat_grp_id]) REFERENCES [plats_grp] ([plat_grp_id]),
    CONSTRAINT [FK_visit_dev_grp] FOREIGN KEY([dev_grp_id]) REFERENCES [devs_grp] ([dev_grp_id]),
    CONSTRAINT [FK_visit_genre] FOREIGN KEY([genre_grp_id]) REFERENCES [genres_grp] ([genre_grp_id]),
)
