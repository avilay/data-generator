from datetime import datetime, timedelta
import logging
import matplotlib.pyplot as plt
from collections import Counter
import numpy as np
import os.path as path
import data_generator.parts.sessions.generator as session_generator
import data_generator.parts.visits.generator as visit_generator


def test_dists():
    visitsfile = '/home/avilay/tmp/visits.csv'
    sessionsfile = '/home/avilay/tmp/sessions.csv'
    if path.exists(visitsfile):
        visits = visit_generator.load(visitsfile)
    else:
        if path.exists(sessionsfile):
            sessions = session_generator.load(sessionsfile)
        else:
            start = datetime(2015, 3, 1)
            end = datetime(2015, 5, 31)
            sessions = session_generator.generate(num_users=100000, tot_sess=2000000,   start=start, end=end, outfile='/home/avilay/tmp/sessions.csv')
        visits = visit_generator.generate(sessions=sessions, tot_visits=6000000, num_prods=10000, outfile='/home/avilay/tmp/visits.csv')
        sessions = None

    print('Generating session distribution histogram')
    sids = [v.session_id for v in visits]
    sids_freq = Counter(sids)
    sess_visits_counts = np.array(list(sids_freq.values()))
    plt.hist(sess_visits_counts)
    plt.xlabel('Visit Counts')
    plt.ylabel('Number of Sessions')
    plt.show()

    print('Generating product distribution')
    pids = [v.product_id for v in visits]
    pids_freq = Counter(pids)
    pids_freq = sorted(pids_freq.items(), key=lambda x: x[1], reverse=True)
    prods = [p[0] for p in pids_freq]
    freqs = [p[1] for p in pids_freq]
    plt.plot(prods, freqs, ',')
    plt.xlabel('Products')
    plt.ylabel('Visit Counts')
    plt.show()


if __name__ == '__main__':
    test_dists()

