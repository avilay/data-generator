from collections import Counter
import numpy as np
import matplotlib.pyplot as plt
import data_generator.sessions.generator as sessgen


def plot_users(sessions):
    print('Generating session distribution by users')
    uids = [sess[1] for sess in sessions]
    # uids_freq = {uid: session count for this user, uid: session count, ...}
    uids_freq = Counter(uids)
    users_sess_counts = np.array(list(uids_freq.values()))
    plt.hist(users_sess_counts)
    plt.xlabel('Session Counts')
    plt.ylabel('Number of Users')
    plt.show()


def plot_attr(sessions, attrname):
    if attrname == 'devices':
        xlabel = 'Devices'
        attrs = [sess[2] for sess in sessions]
    elif attrname == 'browsers':
        xlabel = 'Browsers'
        attrs = [sess[3] for sess in sessions]
    elif attrname == 'plats':
        xlabel = 'OS Platforms'
        attrs = [sess[4] for sess in sessions]
    else:
        raise RuntimeError('Unknown attr {}'.format(attrname))

    valfreq = Counter(attrs)
    valfreq = sorted(valfreq.items(), key=lambda x: x[1], reverse=True)
    vals = [d[0] for d in valfreq]
    freqs = [d[1] for d in valfreq]
    num_bars = len(vals)
    plt.bar(range(num_bars), freqs, align='center')
    plt.xticks(range(num_bars), vals)
    plt.xlabel(xlabel)
    plt.ylabel('Session Counts')
    plt.show()


def plot_created_at(sessions):
    days = [sess[5] for sess in sessions]
    sesscounts_by_day = Counter(days)
    sesscounts_mon = []
    sesscounts_tue = []
    sesscounts_wed = []
    sesscounts_thu = []
    sesscounts_fri = []
    sesscounts_sat = []
    sesscounts_sun = []
    for day, sesscounts in sesscounts_by_day.items():
        if day.weekday() == 0:
            sesscounts_mon.append(sesscounts)
        elif day.weekday() == 1:
            sesscounts_tue.append(sesscounts)
        elif day.weekday() == 2:
            sesscounts_wed.append(sesscounts)
        elif day.weekday() == 3:
            sesscounts_thu.append(sesscounts)
        elif day.weekday() == 4:
            sesscounts_fri.append(sesscounts)
        elif day.weekday() == 5:
            sesscounts_sat.append(sesscounts)
        elif day.weekday() == 6:
            sesscounts_sun.append(sesscounts)
    plt.xlabel('Session Counts')
    plt.ylabel('Number of Mondays')
    plt.hist(sesscounts_mon, bins=7)
    plt.show()

    plt.xlabel('Session Counts')
    plt.ylabel('Number of Tuesdays')
    plt.hist(sesscounts_tue, bins=7)
    plt.show()

    plt.xlabel('Session Counts')
    plt.ylabel('Number of Wednesdays')
    plt.hist(sesscounts_wed, bins=7)
    plt.show()

    plt.xlabel('Session Counts')
    plt.ylabel('Number of Wednesdays')
    plt.hist(sesscounts_wed, bins=7)
    plt.show()

    plt.xlabel('Session Counts')
    plt.ylabel('Number of Thursdays')
    plt.hist(sesscounts_thu, bins=7)
    plt.show()

    plt.xlabel('Session Counts')
    plt.ylabel('Number of Fridays')
    plt.hist(sesscounts_fri, bins=7)
    plt.show()

    plt.xlabel('Session Counts')
    plt.ylabel('Number of Saturdays')
    plt.hist(sesscounts_sat, bins=7)
    plt.show()

    plt.xlabel('Session Counts')
    plt.ylabel('Number of Sundays')
    plt.hist(sesscounts_sun, bins=7)
    plt.show()


def main():
    _, sessions = sessgen.workflow(10000, '/home/avilay/data/ecomm/users.psv', '2015-01-01', '2015-12-31')
    plot_users(sessions)
    plot_attr(sessions, 'devices')
    plot_attr(sessions, 'browsers')
    plot_attr(sessions, 'plats')
    plot_created_at(sessions)


if __name__ == '__main__':
    main()
