import argparse
import numpy as np
from datetime import datetime, timedelta
import dateutil.parser as dtparser
from data_generator.utils import zipf


def setup_cliargs():
    parser = argparse.ArgumentParser()
    parser.add_argument('-n', '--numsess', type=int, help='Number of sessions to generate')
    parser.add_argument('-u', '--users', help='Location of users.psv')
    parser.add_argument('-s', '--start', help='Start date of sessions')
    parser.add_argument('-e', '--end', help='End date of sessions')
    parser.add_argument('outfile', metavar='OUTFILE', help='The location of output file with sessions.')
    return parser


def gen_sess_ids(numsess):
    return np.arange(1, numsess + 1, dtype=int)


def gen_user_ids(numsess, usersfile):
    uids = []
    with open(usersfile, 'rt') as f:
        next(f)  # skip header
        for line in f:
            uids.append(int(line.split('|')[0]))
    numusers = len(uids)

    mu = numsess / numusers
    sigma = mu / 4
    sess_per_user = [int(i) for i in np.random.normal(loc=mu, scale=sigma, size=numusers)]
    totsess = np.sum(sess_per_user)
    if totsess < numsess:
        # pad some users with extra sessions
        d = numsess - totsess
        for i in range(d):
            sess_per_user[i % len(sess_per_user)] += 1
    elif totsess > numsess:
        # strip some users of sessions
        d = totsess - numsess
        for i in range(d):
            sess_per_user[i % len(sess_per_user)] -= 1
    assert np.sum(sess_per_user) == numsess

    user_ids = np.zeros(numsess, dtype=int)
    ndx1 = 0
    for i in range(numusers - 1):
        ndx2 = ndx1 + sess_per_user[i]
        user_ids[ndx1:ndx2] = uids[i]
        ndx1 = ndx2
    if ndx2 < numsess:
        user_ids[ndx2:] = uids[-1]

    np.random.shuffle(user_ids)
    return user_ids


def gen_devices(numsess):
    all_devices = ['laptop', 'tablet', 'desktop', 'phone']
    probs = zipf(len(all_devices))
    devices = np.random.choice(all_devices, p=probs, size=numsess)
    np.random.shuffle(devices)
    return devices


def gen_browsers(numsess):
    all_browsers = ['chrome', 'firefox', 'ie', 'safari', 'other', 'opera']
    probs = zipf(len(all_browsers))
    browsers = np.random.choice(all_browsers, p=probs, size=numsess)
    np.random.shuffle(browsers)
    return browsers


def gen_plats(numsess):
    all_plats = ['windows', 'macos', 'android', 'ios', 'linux']
    probs = zipf(len(all_plats))
    plats = np.random.choice(all_plats, p=probs, size=numsess)
    np.random.shuffle(plats)
    return plats


def gen_created_ats(numsess, start, end):
    created_ats = np.full(numsess, datetime.min, dtype=type(datetime))

    # numsess_by_dow = [tot sess summed across all mondays, tot sess across all tuesdays, ...]
    dow_shares = [0.1447, 0.1504, 0.1559, 0.1577, 0.1453, 0.1275, 0.1184]
    numsess_by_dow = [numsess * dow_share for dow_share in dow_shares]

    # dow_freqs = [number of mondays in the given time interval, number of tuesdays, ...]
    interval = end - start + timedelta(days=1)
    days = [start + timedelta(days=i) for i in range(interval.days)]
    dow_freqs = [0] * 7
    for dow in range(7):
        wkdays = [day for day in days if day.weekday() == dow]
        dow_freqs[dow] = len(wkdays)

    # avgsess_by_dow = [avg num of sess on a monday, avg sess on a tuesday, ...]
    avgsess_by_dow = [nsess / freq for nsess, freq in zip(numsess_by_dow, dow_freqs)]

    # mon_counts = [num sess on 1st monday in the interval, num sess on 2nd monday, ...]
    mon_counts = np.random.poisson(avgsess_by_dow[0], size=dow_freqs[0])
    mon_cur = 0
    tue_counts = np.random.poisson(avgsess_by_dow[1], size=dow_freqs[1])
    tue_cur = 0
    wed_counts = np.random.poisson(avgsess_by_dow[2], size=dow_freqs[2])
    wed_cur = 0
    thu_counts = np.random.poisson(avgsess_by_dow[3], size=dow_freqs[3])
    thu_cur = 0
    fri_counts = np.random.poisson(avgsess_by_dow[4], size=dow_freqs[4])
    fri_cur = 0
    sat_counts = np.random.poisson(avgsess_by_dow[5], size=dow_freqs[5])
    sat_cur = 0
    sun_counts = np.random.poisson(avgsess_by_dow[6], size=dow_freqs[6])
    sun_cur = 0
    ndx1 = 0
    for day in days:
        if day.weekday() == 0:
            counts = mon_counts[mon_cur]
            mon_cur += 1
        elif day.weekday() == 1:
            counts = tue_counts[tue_cur]
            tue_cur += 1
        elif day.weekday() == 2:
            counts = wed_counts[wed_cur]
            wed_cur += 1
        elif day.weekday() == 3:
            counts = thu_counts[thu_cur]
            thu_cur += 1
        elif day.weekday() == 4:
            counts = fri_counts[fri_cur]
            fri_cur += 1
        elif day.weekday() == 5:
            counts = sat_counts[sat_cur]
            sat_cur += 1
        elif day.weekday() == 6:
            counts = sun_counts[sun_cur]
            sun_cur += 1
        ndx2 = ndx1 + counts
        if ndx2 >= numsess:
            created_ats[ndx1:numsess] = day
            break
        else:
            created_ats[ndx1:ndx2] = day
            ndx1 = ndx2
    if ndx2 < numsess:
        created_ats[ndx2:] = days[-1]

    np.random.shuffle(created_ats)
    return created_ats


def workflow(numsess, usersfile, start, end):
    print('Generating {} sessions from {} users between {} and {}'.format(numsess, usersfile, start, end))
    start = dtparser.parse(start)
    end = dtparser.parse(end)
    sess_ids = gen_sess_ids(numsess)
    user_ids = gen_user_ids(numsess, usersfile)
    devices = gen_devices(numsess)
    browsers = gen_browsers(numsess)
    plats = gen_plats(numsess)
    created_ats = gen_created_ats(numsess, start, end)
    sessions = list(zip(sess_ids, user_ids, devices, browsers, plats, created_ats))
    return ['sess_id', 'user_id', 'device', 'browser', 'platform', 'created_at'], sessions


def main():
    args = setup_cliargs().parse_args()
    header, sessions = workflow(args.numsess, args.users, args.start, args.end)
    print('Sessions generated. Writing to file...')
    with open(args.outfile, 'wt') as f:
        print('|'.join(header), file=f)
        for i, session in enumerate(sessions):
            if i % 1000 == 0:
                print('Writing {}th session'.format(i))
            print('|'.join([str(attr) for attr in session]), file=f)
    print('Sessions written to {}'.format(args.outfile))


if __name__ == '__main__':
    main()
