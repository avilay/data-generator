xbox_headers = {
    'title': 0,
    'developers': 1,
    'publishers': 2,
    'released_on': 3,
    'genres': 7
}

xbox360_headers = {
    'title': 0,
    'genres': 1,
    'developers': 2,
    'publishers': 3,
    'released_on': 5
}

xboxone_headers = {
    'title': 0,
    'genres': 1,
    'developers': 2,
    'publishers': 3,
    'released_on': 7
}

xboxkinect_headers = {
    'title': 0,
    'developers': 1,
    'publishers': 2,
    'released_on': 3,
    'genres': -1
}

pc_headers = {
    'title': 0,
    'developers': 1,
    'publishers': 2,
    'genres': 3,
    'released_on': 6
}

gamecube_headers = {
    'title': 0,
    'developers': 1,
    'publishers': 2,
    'released_on': 9,
    'genres': -1
}

wii_headers = {
    'title': 1,
    'developers': 2,
    'publishers': 3,
    'released_on': 10,
    'genres': -1
}

nintendo3ds_headers = {
    'title': 0,
    'developers': 1,
    'publishers': 2,
    'released_on': 10,
    'genres': -1
}

nintendods_headers = {
    'title': 0,
    'released_on': 1,
    'developers': 2,
    'publishers': 3,
    'genres': -1
}

psone_headers = {
    'title': 0,
    'developers': 1,
    'publishers': -1,
    'released_on': 2,
    'genres': -1
}

psmove_headers = {
    'title': 0,
    'developers': 1,
    'publishers': 2,
    'released_on': 6,
    'genres': -1
}

psvita_headers = {
    'title': 0,
    'genres': 1,
    'developers': 2,
    'publishers': 3,
    'released_on': 4
}

psp_headers = {
    'title': 0,
    'released_on': 1,
    'developers': 2,
    'publishers': 3,
    'genres': -1
}

ps4_headers = {
    'title': 0,
    'genres': 1,
    'developers': 2,
    'publishers': 3,
    'released_on': 7
}

ps3_headers = {
    'title': 0,
    'developers': 1,
    'publishers': -1,
    'released_on': 5,
    'genres': -1
}

ps2_headers = {
    'title': 0,
    'released_on': 1,
    'publishers': 2,
    'developers': 3,
    'genres': -1
}
