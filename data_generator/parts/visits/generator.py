import csv
import os.path as path
import logging
from datetime import timedelta
import dateutil.parser as dtparser
import numpy as np
from data_generator.utils import IdGenerator

logger = logging.getLogger(__name__)


class Visit:
    def __init__(self, **kwargs):
        self.visit_id = kwargs['visit_id']
        self.session_id = kwargs['session_id'] if 'session_id' in kwargs else None
        self.user_id = kwargs['user_id']
        self.product_id = kwargs['product_id'] if 'product_id' in kwargs else None
        self.genre_grp_id = kwargs['genre_grp_id'] if 'genre_grp_id' in kwargs else None
        self.pub_grp_id = kwargs['pub_grp_id'] if 'pub_grp_id' in kwargs else None
        self.plat_grp_id = kwargs['plat_grp_id'] if 'plat_grp_id' in kwargs else None
        self.dev_grp_id = kwargs['dev_grp_id'] if 'dev_grp_id' in kwargs else None
        self.started_at = kwargs['started_at'] if 'started_at' in kwargs else None
        self.dur_secs = kwargs['dur_secs'] if 'dur_secs' in kwargs else None


def load(filepath):
    logger.debug('Loading visits from file')
    cachefile = path.join(filepath, 'visits.csv')
    visits = []
    with open(cachefile, 'rt') as f:
        reader = csv.reader(f)
        next(reader)
        for visit in reader:
            visits.append(Visit(
                visit_id=int(visit[0]),
                session_id=int(visit[2]),
                product_id=int(visit[3]),
                started_at=dtparser.parse(visit[4]),
                ended_at=dtparser.parse(visit[5])
            ))
    return visits


def generate(sessions, tot_visits, games, outfile):
    logger.debug('Generating new visits')
    mu = tot_visits / len(sessions)
    sigma = mu / 4

    num_prods = len(games)
    probs = np.sort(np.random.exponential(scale=0.5, size=num_prods))[::-1]
    prod_probs = probs / np.sum(probs)

    all_visits = []

    # with open(outfile, 'wt') as f:
    #     writer = csv.writer(f)
        # writer.writerow(['visit_id', 'session_id', 'user_id', 'game_id', 'genre_grp_id', 'pub_grp_id', 'plat_grp_id', 'dev_grp_id', 'start_time', 'dur_secs'])

    for session in sessions:
        id_gen = IdGenerator()

        num_visits = np.random.normal(loc=mu, scale=sigma)
        num_visits = round(max(0, num_visits))

        last_visit_ended_at = session.created_at
        for _ in range(num_visits):
            game = np.random.choice(a=games, p=prod_probs)
            started_at = last_visit_ended_at + timedelta(seconds=np.random.randint(3, 59))
            dur_in_secs = np.random.randint(30, 180)  # anywhere between 30 secs to 3 mins
            ended_at = started_at + timedelta(seconds=dur_in_secs)
            last_visit_ended_at = ended_at
            all_visits.append(Visit(
                visit_id=session.session_id * 100 + id_gen.generate(),
                session_id=session.session_id,
                user_id=session.user_id,
                product_id=game.product_id,
                genre_grp_id=game.genre_grp_id,
                pub_grp_id=game.pub_grp_id,
                plat_grp_id=game.plat_grp_id,
                dev_grp_id=game.dev_grp_id,
                started_at=started_at,
                dur_secs=dur_in_secs
            ))

        if len(all_visits) >= 500000:
            logger.debug('Writing {} visits to {}'.format(len(all_visits), outfile))
            with open(outfile, 'at') as f:
                writer = csv.writer(f)
                for v in all_visits:
                    writer.writerow([v.visit_id, v.session_id, v.user_id, v.product_id, v.genre_grp_id, v.pub_grp_id, v.plat_grp_id, v.dev_grp_id, v.started_at, v.dur_secs])
            all_visits = []
            logger.debug('Write done')

    logger.debug('Writing {} visits to {}'.format(len(all_visits), outfile))
    with open(outfile, 'at') as f:
        writer = csv.writer(f)
        for v in all_visits:
            writer.writerow([v.visit_id, v.session_id, v.user_id, v.product_id, v.genre_grp_id, v.pub_grp_id, v.plat_grp_id, v.dev_grp_id, v.started_at, v.dur_secs])
    logger.debug('Write done')

