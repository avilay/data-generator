import argparse
import numpy as np
from datetime import datetime
import dateutil.parser as dtparser


def gen_visit_ids(numvisits):
    vids = np.arange(1, numvisits + 1, dtype=int)
    # return np.reshape(vids, (numvisits, 1))
    return vids


def gen_sess(numvisits, sessfile, numsess):
    """
    The session file must be in the following format -
    sess_id|user_id|device|browser|platform|created_at
    :param numvisits: number of visits to generate
    :param sessfile: pipe-delimited text file with session data
    :param numsess: number of sessions in the session file
    :return: An integer matrix with 4 columns - session_id, user_id, visit_start_time epoch, duration_in_secs
    and numvisits number of rows. Each session appears in the visit multiple times based on a probability
    distribution.
    """
    sdata = np.zeros((numsess, 3), dtype=int)
    with open(sessfile, 'rt') as f:
        next(f)  # skip header
        for ndx, line in enumerate(f):
            line = line.strip()
            flds = line.split('|')
            sid = int(flds[0])
            uid = int(flds[1])
            cat = dtparser.parse(flds[5])
            sdata[ndx] = [sid, uid, cat.timestamp()]

    mu = numvisits / numsess
    sigma = mu / 4
    visits_per_sess = [int(i) for i in np.random.normal(loc=mu, scale=sigma, size=numsess)]
    totvisits = np.sum(visits_per_sess)
    if totvisits < numvisits:
        # pad some sessions with extra visits
        d = numvisits - totvisits
        for i in range(d):
            visits_per_sess[i % len(visits_per_sess)] += 1
    elif totvisits > numvisits:
        # strip some sessions of visits
        d = totvisits - numvisits
        for i in range(d):
            visits_per_sess[i % len(visits_per_sess)] -= 1
    assert np.sum(visits_per_sess) == numvisits

    sess = np.zeros((numvisits, 4), dtype=int)
    dur_per_visit = np.random.randint(30, 180, size=numvisits)
    ndx1 = 0
    for i in range(numsess - 1):
        ndx2 = ndx1 + visits_per_sess[i]
        sess[ndx1:ndx2, 0:3] = sdata[i, :]
        durs = dur_per_visit[ndx1:ndx2]
        sess[ndx1:ndx2, 3] = durs
        cumdurs = np.cumsum(durs)
        sess[ndx1:ndx2, 2] += cumdurs
        ndx1 = ndx2
    if ndx2 < numvisits:
        sess[ndx2:, 0:3] = sdata[-1, :]
        durs = dur_per_visit[ndx2:]
        sess[ndx2:, 3] = durs
        cumdurs = np.cumsum(durs)
        sess[ndx2:, 2] += cumdurs

    return sess


def gen_games(numvisits, gamesfile, numgames):
    """
    The games file must be in the following format -
    game_id|title|released_on|price|pub_grp_id|plat_grp_id|dev_grp_id|genre_grp_id
    :param numvisits: number of visits to generate
    :param gamesfile: pipe-delimited text file with games info
    :param numgames: number of games in the games file
    :return: An integer matrix with 5 columns - game_id, pub_grp_id, plat_grp_id, dev_grp_id, genre_grp_id
    and numvisits number of rows.
    Each game appears multiple times in the return matrix based on its probability.
    """
    gdata = np.zeros((numgames, 5), dtype=int)
    with open(gamesfile, 'rt') as f:
        next(f)
        for ndx, line in enumerate(f):
            line = line.strip()
            flds = line.split('|')
            gid = int(flds[0])
            pub_grp_id = int(flds[4]) if flds[4] else -1
            plat_grp_id = int(flds[5]) if flds[5] else -1
            dev_grp_id = int(flds[6]) if flds[6] else -1
            genre_grp_id = int(flds[7]) if flds[7] else -1
            gdata[ndx] = [gid, pub_grp_id, plat_grp_id, dev_grp_id, genre_grp_id]
    expectations = np.sort(np.random.exponential(scale=0.5, size=numgames))[::-1]
    probs = expectations / np.sum(expectations)
    ndxs = np.random.choice(np.arange(numgames), p=probs, size=numvisits)
    games = gdata[ndxs]
    return games


def workflow(numvisits, sessfile, numsess, gamesfile, numgames, outfile):
    visit_ids = gen_visit_ids(numvisits)
    sess = gen_sess(numvisits, sessfile, numsess)
    np.random.shuffle(sess)
    games = gen_games(numvisits, gamesfile, numgames)
    # visits = np.hstack((visit_ids, sess, games))
    header = ['visit_id', 'session_id', 'user_id', 'start_time', 'dur_in_secs', 'game_id', 'pub_grp_id', 'plat_grp_id', 'dev_grp_id', 'genre_grp_id']
    print('Visits generated. Writing to {}'.format(outfile))
    with open(outfile, 'wt') as f:
        print('|'.join(header), file=f)
        for row in range(numvisits):
            if row % 1000 == 0:
                print('Writing {}th visit'.format(row))
            line = []
            line.append(str(visit_ids[row]))  # visit_id
            line.append(str(sess[row, 0]))  # session_id
            line.append(str(sess[row, 1]))  # user_id
            line.append(str(datetime.fromtimestamp(sess[row, 2])))  # start_time
            line.append(str(sess[row, 3]))  # dur_in_secs
            line.append(str(games[row, 0]))  # game_id
            pub_grp_id = str(games[row, 1]) if games[row, 1] != -1 else ''
            line.append(pub_grp_id)
            plat_grp_id = str(games[row, 2]) if games[row, 2] != -1 else ''
            line.append(plat_grp_id)
            dev_grp_id = str(games[row, 3]) if games[row, 3] != -1 else ''
            line.append(dev_grp_id)
            genre_grp_id = str(games[row, 4]) if games[row, 4] != -1 else ''
            line.append(genre_grp_id)
            print('|'.join(line), file=f)
    print('Visits written to file')


def setup_cliargs():
    parser = argparse.ArgumentParser()
    parser.add_argument('-v', '--numvisits', type=int, help='Number of visits to generate')
    parser.add_argument('-s', '--numsess', type=int, help='Number of sessions in the session file')
    parser.add_argument('-S', '--sessions', help='Location of sessions.psv')
    parser.add_argument('-g', '--numgames', type=int, help='Number of games in the games file')
    parser.add_argument('-G', '--games', help='Location of games.psv')
    parser.add_argument('outfile', metavar='OUTFILE', help='The location of output file with visits.')
    return parser


def main():
    args = setup_cliargs().parse_args()
    print('Generating {} visits'.format(args.numvisits))
    workflow(args.numvisits, args.sessions, args.numsess, args.games, args.numgames, args.outfile)


if __name__ == '__main__':
    main()


